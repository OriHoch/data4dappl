import sys
from dataflows_shell.processors.ckan import ckan
from dataflows import Flow, load
from datapackage_pipelines.utilities.resources import PROP_STREAMING
from datapackage_pipelines.wrapper import ingest, spew


def get_group_users_to_create(entities, all_organization_user_ids):
    for entity in entities:
        entity_user_ids = [entity_user['id'] for entity_user in entity['users']]
        for organiztion_user_id in all_organization_user_ids:
            if organiztion_user_id not in entity_user_ids or (organiztion_user_id == 'dc13a932-16ef-414b-a126-028ee21679cb' and entity['id'] == '48da99ad-5e27-48f7-b8b4-3ee460f4ebfb'):
                yield {'object': organiztion_user_id,
                       'object_type': 'user',
                       'id': entity['id'],
                       'capacity': 'member'}


def get_create_member_params(package):
    package.pkg.remove_resource('ckan_organizations')
    package.pkg.remove_resource('ckan_groups')
    package.pkg.add_resource({"name": "group_users_to_create", "path": "group_users_to_create.csv",
                              PROP_STREAMING: True,
                              "schema": {"fields": [{"name": "id", "type": "string"},
                                                    {"name": "object", "type": "string"},
                                                    {"name": "object_type", "type": "string"},
                                                    {"name": "capacity", "type": "string"}]}})
    yield package.pkg
    all_organization_user_ids = set()
    for rows in package:
        if rows.res.name == 'ckan_organizations':
            for org in rows:
                for org_user in org['users']:
                    all_organization_user_ids.add(org_user['id'])
        elif rows.res.name == 'ckan_groups':
            yield get_group_users_to_create(rows, all_organization_user_ids)
        else:
            yield rows


if __name__ == '__main__':
    parameters, datapackage, resources = ingest()
    ds = Flow(load((datapackage, resources)), get_create_member_params).datastream()
    ds.dp.descriptor['name'] = '_'
    spew(ds.dp.descriptor, ds.res_iter)
